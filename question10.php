/*
 *  Question 10
 *  Author: Erik Garcia
 */
<?php //php 7.0.8

$files = [
    '/usr/share/nginx/wordpress/wp-content/themes/index.php',
    '/usr/share/nginx/wordpress/wp-content/themes/mytheme.php',
    '/usr/share/nginx/wordpress/wp-content/plugins/myplugin.php',
    '/usr/share/nginx/wordpress/wp-content/plugins/akismet.php',
    '/usr/share/nginx/wordpress/wp-content/uploads/november.jpg',
];

$exclude = [
    '/usr/share/nginx/wordpress/wp-content/uploads',
    '/usr/share/nginx/wordpress/wp-content/plugins/myplugin.php',
];

$ePaths = $eFiles = array();
foreach($exclude as $item)
{
    $info = pathinfo($item);
    empty($info['extension']) ? $ePaths[] = $item: $eFiles[] = $item;
}

$results = array();
foreach($files as $file)
{
    
    if(!(in_array(dirname($file), $ePaths) || in_array($file, $eFiles)))
    {
        $results[] = $file;
    }
}
print_r($results );
?>