/*
 *  Question 4
 *  Author: Erik Garcia
 */
<?php //php 7.0.8
    // The fuction
    function search($int)
    {
        $integers = [0,1,2,3,4,5,6,7,8,9];
        return in_array($int, $integers);
    }

    // The exexutions
    echo "First Execution: ". (search(0)? "TRUE": "FALSE"). "\n";
    echo "Second Execution: ". (search(8)? "TRUE": "FALSE"). "\n";
    echo "Third Execution: ". (search(18)? "TRUE": "FALSE"). "\n";
?>