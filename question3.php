/*
 *  Question 3
 *  Author: Erik Garcia
 */
<?php //php 7.0.8

    $options = [
        'salt' => 'SECURITY_STUFF_RANDOM_', //write your own code to generate a suitable salt
        'cost' => 12 // the default cost is 10
    ];

    // A password
    $password = 'a_secret_one';

    // Hashing password
    $hash = password_hash($password, PASSWORD_DEFAULT, $options);
    echo "The password: {$password}\n";
    echo "Hashed password: {$hash}\n";
    if (password_verify($password, $hash)) {
        echo 'SUCCEED!!!';
    }
    else {
         echo 'Invalid';
    }
?>