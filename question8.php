/*
 *  Question 8
 *  Author: Erik Garcia
 */
<?php
use PHPUnit\Framework\TestCase;

final class FizzBuzzTest extends TestCase
{

    function fizzBuzz($start = 1, $stop = 100) {
        $string = '';
        if ($stop < $start || $start < 0 || $stop < 0) { throw new InvalidArgumentException();}
        for ($i = $start; $i <= $stop; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) { $string .= 'FizzBuzz'; continue; }
            if ($i % 3 == 0) { $string .= 'Fizz'; continue; }
            if ($i % 5 == 0) { $string .= 'Buzz'; continue; }
            $string .= $i;
        }
        return $string;
    }

    public function testCannotBeStartBiggerThanStop() {
        $this->expectException(InvalidArgumentException::class);
        $this->fizzBuzz(2,1);
    }

    public function testStartIsPositiveNumber() {
        $this->expectException(InvalidArgumentException::class);
        $this->fizzBuzz(-1,1);
    }

    public function testStopIsPositiveNumber(){
        $this->expectException(InvalidArgumentException::class);
        $this->fizzBuzz(1,-1);
    }

    public function testFizzBuzz() {
        $this->assertEquals($this->fizzBuzz(15,15),'FizzBuzz');
    }

    public function testFizz() {
        $this->assertEquals($this->fizzBuzz(3,3),'Fizz');
    }

    public function testBuzz() {
        $this->assertEquals($this->fizzBuzz(5,5), 'Buzz');
    }

    public function testBuzzPreviousNumber() {
        $this->assertEquals($this->fizzBuzz(4,5), '4Buzz');
    }

    public function testBuzzFizz() {
        $this->assertEquals($this->fizzBuzz(5,6), 'BuzzFizz');
    }

    public function testBuzzFizzNextNumber() {
        $this->assertEquals($this->fizzBuzz(5,7), 'BuzzFizz7');
    }
}
?>