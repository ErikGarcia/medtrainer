/*
 *  Question 5
 *  Author: Erik Garcia
 */
<?php
    // Create connection
    $conn = new mysqli("localhost", "root", "garcia81");
    
    // Check connection
    if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);

    // Getting the total of rows in table
    $result = $conn->query("SELECT count(1) as counter FROM seguridad_proyectos.users");
    $counter = $result->fetch_assoc();
    $nitems = $counter['counter'];

    //Getting the results by lot
    $byIterator = 100;
    $niterations = ceil($nitems/$byIterator);
    for($i = 0; $i < $niterations; $i++)
    {
        echo "<pre>". print_r(array(
            'i' => $i, 
            'byIterator' => $byIterator
        ), true). "</pre>";

        $sql = "SELECT * FROM seguridad_proyectos.users LIMIT " . ($i*$byIterator) . ", {$byIterator}";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                #echo "<pre>".print_r($row, true). "</pre>";
                echo print_r($row, true). "<br/>";
            }
        }
    }
    $conn->close();
?>