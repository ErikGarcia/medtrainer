/*
 *  Question 6
 *  Author: Erik Garcia
 */
<?php //php 7.0.8

    function phone_sanityzation($phone, $delimiter = '-')
    {
        $tmp = $phone;
        $delimiters = array(" ", "(", ")", $delimiter);
        $phone = str_replace($delimiters, "", $phone);
        echo $phone;
        
        return array(
            'original' => $tmp,
            // 3-3-4 Format
            'phone' => substr($phone, 0, 3) . '-'. substr($phone, 3, 3) . '-' . substr($phone, 6, 4),
            'isValid' => (is_numeric($phone) && strlen($phone) == 10)
        );
    }
    
    $phones = array('123-456-7890', '(123) 456-7890', '234567890');
    $formatted = array();
    foreach($phones as $phone)
    {
        $formatted[] = phone_sanityzation($phone);
    }
    print_r($formatted);
?>